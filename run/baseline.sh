#!/bin/sh

OMP_NUM_THREADS=1 python -m src.run \
	model/nn=baseline.yaml \
	logging.wandb.resume=BaseLine \
	train.checkpoint.file=baseline.check \
	train.val.video.dir=video_baseline \
	train.checkpoint.interval=1 \
	train.reps=10 \
	data.train.n_envs=100 \
	train.episodes=200 \
	model.std_dev=0.1 \
	data.train.use_backgrounds=False \
	+data.train.distribution_mode=easy \
	data.val.use_backgrounds=False \
	+data.val.distribution_mode=easy \
	model.jobs=5
