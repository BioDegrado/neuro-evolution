#!/bin/sh

python -m src.run \
	train.checkpoint.file=battlezone.check \
	train.debug=False \
	data=battlezone \
	logging.wandb.resume=BattleZone \
	train.val.video.dir=video_battlezone \
	train.batch=1 \
	train.reps=5 \
	model.jobs=2 \
