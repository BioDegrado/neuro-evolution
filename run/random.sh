#!/bin/sh

OMP_NUM_THREADS=1 python -m src.run \
	model=simple \
	logging.wandb.resume=Random \
	train.checkpoint.file=random.check \
	train.val.video.dir=video_random \
	train.checkpoint.interval=100 \
	train.reps=10 \
	train.population=1 \
	data.train.n_envs=100 \
	train.episodes=500 \
