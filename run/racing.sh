#!/bin/sh

python -m src.run \
	data=racing \
	logging.wandb.resume=Racing \
	train.checkpoint.file=racing.check \
	train.val.video.dir=video_racing \
	model/nn=psann.yaml \
	train.checkpoint.interval=10 \
	train.episodes=200 \
	train.reps=16 \
	model.nn.num_layers=1 \
	model.nn.hidden_size=16 \
	model.nn.patch_size=7 \
	model.nn.stride=4 \
	model.nn.input_size=96 \
	data.train.n_envs=200 \
	model.jobs=10 \
