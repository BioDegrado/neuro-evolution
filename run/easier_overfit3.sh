#!/bin/sh

OMP_NUM_THREADS=1 python -m src.run \
	model/nn=psann.yaml \
	logging.wandb.resume=Coinrun_overfit_psann3 \
	train.checkpoint.file=coinrun_overfit_psann3.check \
	train.val.video.dir=video_coinrun_overfit_easier3 \
	train.checkpoint.interval=5 \
	train.reps=5 \
	data.train.n_envs=5 \
	train.episodes=1000 \
	model.std_dev=0.08 \
	model.nn.num_layers=1 \
	model.nn.hidden_size=16 \
	model.nn.patch_size=7 \
	model.nn.stride=4 \
	model.nn.input_size=96 \
	data.env_name="procgen:procgen-coinrun-v0" \
	data.train.use_backgrounds=False \
	+data.train.distribution_mode=easy \
	model.jobs=9
	# +data.train.restrict_themes=True \
	# +data.train.use_monochrome_assets=True \
	# +data.val.restrict_themes=True \
	# +data.val.use_monochrome_assets=True \
	# data.val.use_backgrounds=False \
	# +data.val.distribution_mode=easy \
