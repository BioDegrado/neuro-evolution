#!/bin/sh

python -m src.run \
	train.checkpoint.file=debug.check \
	train.debug=True \
	logging.wandb.resume=Debug \
	train.val.video.dir=video_debug \
	train.batch=1 \
	train.reps=1 \
	train.population=10 \
	train.val.interval=1 \
	data.val.size=5 \
	model.jobs=2 \
