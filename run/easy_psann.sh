#!/bin/sh

OMP_NUM_THREADS=1 python -m src.run \
	model/nn=psann.yaml \
	logging.wandb.resume=PSANN23 \
	train.checkpoint.file=psann23.check \
	train.val.video.dir=video_psann23 \
	train.checkpoint.interval=10 \
	train.reps=10 \
	data.train.n_envs=100 \
	train.episodes=500 \
	model.std_dev=0.1 \
	model.nn.num_layers=1 \
	model.nn.hidden_size=16 \
	model.nn.patch_size=5 \
	model.nn.stride=3 \
	data.train.use_backgrounds=False \
	+data.train.distribution_mode=easy \
	data.val.use_backgrounds=False \
	+data.val.distribution_mode=easy \
	model.jobs=10
