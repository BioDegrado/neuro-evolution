#!/bin/sh

OMP_NUM_THREADS=1 python -m src.run \
	model/nn=psann \
	data=simple \
	logging.wandb.resume=Breakout \
	train.checkpoint.file=breakout.check \
	train.val.video.dir=video_breakout \
	train.checkpoint.interval=10 \
	train.reps=10 \
	train.episodes=500 \
	train.population=128 \
	data.env_name=Breakout-v0 \
	data.train.n_envs=200 \
	model.std_dev=0.1 \
	model.nn.input_size=96 \
	model.nn.num_layers=1 \
	model.nn.hidden_size=16 \
	model.nn.patch_size=7 \
	model.nn.stride=4 \
	model.jobs=6
