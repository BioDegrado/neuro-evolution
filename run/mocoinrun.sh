#!/bin/sh

OMP_NUM_THREADS=1 python -m src.run \
	data.env_name="procgen:procgen-coinrun-v0" \
	model/nn=mopsann.yaml \
	logging.wandb.resume=CoinRun_MOPSANN \
	train.checkpoint.file=coinrun_mopsann.check \
	train.val.video.dir=video_coinrun_mopsann \
	train.checkpoint.interval=10 \
	train.reps=10 \
	data.train.n_envs=100 \
	train.episodes=500 \
	model.std_dev=0.1 \
	model.nn.num_layers=1 \
	model.nn.hidden_size=16 \
	model.nn.patch_size=5 \
	model.nn.stride=3 \
	model.nn.p_class_out=3 \
	model.jobs=8

