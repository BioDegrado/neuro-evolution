#!/bin/sh

OMP_NUM_THREADS=1 python -m src.run \
	data.env_name="procgen:procgen-coinrun-v0" \
	model/nn=psann.yaml \
	logging.wandb.resume=CoinRun_PSANN3 \
	train.checkpoint.file=coinrun_psann3.check \
	train.val.video.dir=video_coinrun_psann3 \
	train.checkpoint.interval=10 \
	train.reps=16 \
	data.train.n_envs=500 \
	train.episodes=500 \
	train.population=256 \
	model.std_dev=0.1 \
	model.nn.input_size=64 \
	model.nn.num_layers=1 \
	model.nn.selfattention_size=4 \
	model.nn.hidden_size=16 \
	model.nn.patch_size=7 \
	model.nn.stride=4 \
	model.jobs=10
