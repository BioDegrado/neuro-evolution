#!/bin/sh

OMP_NUM_THREADS=1 HYDRA_FULL_ERROR=1 python -m src.run \
	model/nn=mopsann.yaml \
	logging.wandb.resume=LargeMoBossFightCMA13 \
	train.checkpoint.file=mobossfight13.check \
	train.val.video.dir=video_mopsann13 \
	train.checkpoint.interval=1 \
	train.reps=16 \
	data.train.n_envs=200 \
	train.episodes=500 \
	model.std_dev=0.1 \
	model.nn.stride=4 \
	model.nn.patch_size=7 \
	model.nn.input_size=64 \
	model.nn.selfattention_size=8 \
	model.nn.num_layers=1 \
	model.nn.hidden_size=32 \
	data.train.use_backgrounds=False \
	+data.train.distribution_mode=easy \
	data.val.use_backgrounds=False \
	+data.val.distribution_mode=easy \
	model.jobs=5

