#!/bin/sh

OMP_NUM_THREADS=1 python -m src.run \
	model/nn=psann \
	logging.wandb.resume=PSANN25 \
	train.checkpoint.file=video_psann25.check \
	train.val.video.dir=video_psann25 \
	train.checkpoint.interval=10 \
	train.reps=16 \
	train.episodes=500 \
	data.train.n_envs=200 \
	model.std_dev=0.1 \
	model.nn.input_size=96 \
	model.nn.num_layers=1 \
	model.nn.hidden_size=16 \
	model.nn.patch_size=7 \
	model.nn.stride=4 \
	model.jobs=10
