import unittest
import cv2
import gym

import numpy as np
import torch
from torch import nn
from src.modules.mopsann import MOPSANN
from src.modules.actor import Actor

class Test(unittest.TestCase):
    def setUp(self):
        torch.set_default_dtype(torch.double)
        color_gradient = torch.tensor(np.arange(0, 1, 1/96))
        r = color_gradient[:, None] + torch.zeros(96)[None, :]
        g = color_gradient[None, :] + torch.zeros(96)[:, None]
        b = torch.zeros(96, 96) + 0.5
        img = torch.stack([b, g, r])
        self.img = img

        ac_space = gym.make("procgen:procgen-bossfight-v0").action_space

        self.model = MOPSANN(action_space=ac_space)
        self.model_env = MOPSANN(env_name="MsPacman-v0")
        self.model_box = MOPSANN(env_name="CarRacing-v0")

    def test_patch_classifier(self):
        print(self.img.shape)
        patches = self.model.gen_patches(self.img)
        print(patches.shape)
        patches = patches.transpose(1,2).squeeze() # torch.einsum('bms->bsm', x)
        k_patches = patches[:10]
        print(k_patches.shape)
        res = self.model.patch_classifier(k_patches)
        print(res.shape)

    def test_config(self):
        self.assertIsInstance(self.model, Actor)
        self.assertIsInstance(self.model_env, Actor)
        self.assertIsInstance(self.model_box, Actor)

        config = self.model.get_config()
        cfg_env = self.model_env.get_config()
        cfg_box = self.model_box.get_config()

        copy = MOPSANN(**config)
        cp_env = MOPSANN(**cfg_env)
        cp_box = MOPSANN(**cfg_box)

        self.assertDictEqual(copy.get_config(), self.model.get_config())
        self.assertDictEqual(cp_env.get_config(), self.model_env.get_config())
        self.assertDictEqual(cp_box.get_config(), self.model_box.get_config())


    def test_forward_memory(self):
        oki = []
        for _ in range(256):
            oki.append(self.model(self.img))


    def test_generatepatches_shape(self):
        unfolder = nn.Unfold(kernel_size=7, stride=4)
        img = self.img
        patches = unfolder(img.view(1,3, 96, 96))
        patches = patches.view(patches.shape[1], -1)
        patches = torch.einsum('ms->sm', patches)
        # img_patch = torch.einsum('cwh->whc', patches[23*22].view(3,7,7)).numpy()
        #  cv2.imshow("img", self.img.numpy())
        #  cv2.imshow("img", img_patch)
        #  cv2.waitKey(0)

        self.assertEqual(patches.shape, (529, 147))

    def test_forward(self):
        y = self.model(self.img)
        self.assertEqual(y.shape[0], 15)

    def test_forward_box(self):
        y = self.model_box(self.img)
        self.assertEqual(y.shape[0], 3)

    def test_forward_env(self):
        y = self.model_env(self.img)
        self.assertEqual(y.shape[0], 9)


def main() -> None:
    test = Test()

if __name__ == "__main__":
    main()
