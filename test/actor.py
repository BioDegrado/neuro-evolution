import unittest
import cv2
import gym

import numpy as np
import torch
from src.modules.mopsann import MOPSANN
from src.modules.psann import PSANN
from src.modules.actor import Actor

class Test(unittest.TestCase):
    def setUp(self):
        torch.set_default_dtype(torch.double)
        color_gradient = torch.tensor(np.arange(0, 1, 1/96))
        r = color_gradient[:, None] + torch.zeros(96)[None, :]
        g = color_gradient[None, :] + torch.zeros(96)[:, None]
        b = torch.zeros(96, 96) + 0.5
        img = torch.stack([b, g, r])
        self.img = img

        ac_space = gym.make("procgen:procgen-bossfight-v0").action_space

        self.model: Actor = PSANN(action_space=ac_space)


    def test_config(self):
        self.assertIsInstance(self.model, Actor)
        config = self.model.get_config()
        copy = PSANN(**config)
        self.assertDictEqual(copy.get_config(), self.model.get_config())


    def test_forward_memory(self):
        oki = []
        for _ in range(256):
            oki.append(self.model(self.img))


    def test_device(self):
        self.assertIsNotNone(self.model.get_device())


    def test_forward(self):
        y = self.model(self.img)
        self.assertEqual(y.shape[0], 15)



    def test_parameters_changes(self):
        model= self.model
        params = model.get_parameters()
        model.set_parameters(params)
        params2 = model.get_parameters()
        self.assertTrue(torch.equal(params, params2))

        rparams = torch.randn_like(params2)
        model.set_parameters(rparams)
        oki = model.get_parameters()
        self.assertTrue(torch.equal(rparams, oki))


def main() -> None:
    test = Test()

if __name__ == "__main__":
    main()
