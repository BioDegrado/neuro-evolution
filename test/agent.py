import unittest

import os
import torch
import numpy as np

from src.common import agent
from src.common.env import postprocess_discrete
from src.modules.psann import PSANN
from src.modules.mopsann import MOPSANN

from src.common.env import Env

class Test(unittest.TestCase):
    def setUp(self):
        torch.set_default_dtype(torch.double)
        self.env = Env("procgen:procgen-coinrun-v0")
        self.actor = PSANN(
            env_name = "procgen:procgen-coinrun-v0",
            k_patches = 10,
            patch_size = 7,
            stride = 4,
            hidden_size = 16,
            input_size = 96,
            selfattention_size = 4,
        )
        self.actor2 = MOPSANN(
            env_name = "procgen:procgen-coinrun-v0", 
            k_patches = 10,
            patch_size = 7,
            stride = 4,
            hidden_size = 16,
            input_size = 96,
            selfattention_size = 4,
        )

        color_gradient = torch.arange(0, 1, 1/96)
        r = color_gradient[:, None] + torch.zeros(96)[None, :]
        g = color_gradient[None, :] + torch.zeros(96)[:, None]
        b = torch.zeros(96, 96) + 0.5
        # img = torch.stack([b, g, r])
        img = torch.stack([r, g, b])
        # self.img = img
        self.img = np.array(torch.einsum("chw->hwc", img))


    def test_get_action(self):
        for _ in range(10):
            action = agent.get_action(self.actor, np.array(self.env.reset()))
            action = postprocess_discrete(action)
            self.assertTrue(self.env.action_space.contains(action))


    def test_to_tensor(self):
        #  print(self.img.shape)
        img = agent._to_tensor(self.img)
        #  print(img.shape)
        #  plt.imshow(img.permute(1,2,0))
        #  plt.show()

        patches = self.actor.gen_patches(img)
        patches = patches.transpose(1,2) # torch.einsum('bms->bsm', x)
        scores = self.actor.selfattention(patches)
        print(scores)


    def test_play_discrete(self):
        reward = agent.play_episode(self.actor, self.env)
        self.assertIsNotNone(reward)
        print(f"Reward: {reward}")


    def test_play_box(self):
        actor = PSANN(env_name = "CarRacing-v0")
        racing = Env("CarRacing-v0")
        reward = agent.play_episode(actor, racing)
        self.assertIsNotNone(reward)
        print(f"Reward: {reward}")


    def test_video(self):
        video="test/check/video.avi"
        video2="test/check/video2.avi"
        os.remove(video) if os.path.exists(video) else None
        os.remove(video2) if os.path.exists(video2) else None
        self.assertFalse(os.path.exists(video))
        self.assertFalse(os.path.exists(video2))
        reward = agent.record_episode(
            self.actor,
            self.env,
            record_file=video
        )
        reward = agent.record_episode(
            self.actor2,
            self.env,
            record_file=video2
        )
        self.assertTrue(os.path.isfile(video))
        self.assertTrue(os.path.isfile(video2))

        statp = os.stat(video).st_mtime

        reward = agent.record_episode(
            self.actor,
            self.env,
            record_file=video
        )

        stata = os.stat(video).st_mtime
        self.assertNotEqual(stata, statp)


if __name__ == "__main__":
    test = Test()
