import unittest

from torch.utils.data import DataLoader
from torch.utils.data import random_split
import cma

from pathlib import Path
from omegaconf import OmegaConf
from src.models.cma_model import CMAModel
from src.modules.psann import PSANN

from src.data.dataset import EnvDataset
from src.common.utils import PROJECT_ROOT
from src.common.utils import simple_collate


class Test(unittest.TestCase):

    def setUp(self):
        self.env_conf = OmegaConf.create({
            "id": "procgen:procgen-bossfight-v0",
            "num_levels": 1,
            "use_sequential_levels": True
        })
        self.check_conf = OmegaConf.create({
            "resume": False,
            "interval": 10,
            "dir": "test/check",
            "file": "check.check",
        })
        self.val_conf = OmegaConf.create({
            "interval": 10,
            "episodes": 10,
            "video": OmegaConf.create({
                "dir": "../test_video",
                "resolution": 500
            })
        })
        self.cma = cma.CMAEvolutionStrategy([0] * 10, sigma0=0.1)
        self.psann = PSANN(env_name="procgen:procgen-bossfight-v0")
        self.model = CMAModel(
            env_name=self.env_conf.id,
            actor = self.psann,
            population=5,
            checkpoint=self.check_conf,
            validation=self.val_conf,
            reps=3,
            jobs=5,
        )
        dataset = EnvDataset(20, "procgen:procgen-bossfight-v0")
        train, val = random_split(dataset, [15, 5])
        self.train_loader = DataLoader(train, batch_size=5 * 3, collate_fn=simple_collate)
        self.val_loader = DataLoader(val, batch_size=len(val), collate_fn=simple_collate)


    def test_fit(self):
        self.model.fit(self.train_loader, self.val_loader)
        print("terminated test_fit")


    def test_checkpoint(self):
        l = [20, 20, 20]
        self.model.save_state(10, l, l, "")

        i = 30
        rew = [100, 100, 100]
        self.model.save_state(i, rew, rew, "")
        oki = self.model.load_state("")
        self.assertIsNotNone(oki)
        if oki is not None:
            self.assertEqual(i, oki["iteration"])
            self.assertEqual(rew, oki["train_rewards"])
            self.assertEqual(rew, oki["val_rewards"])
        print("terminated test_checkpoint")


    def test_test(self):
        path = Path(self.val_conf.video.dir)
        self.val_conf.video.path = str(path if path.is_absolute() else PROJECT_ROOT / path)
        i = 0
        for batch in self.val_loader:
            self.model.test(self.model.get_candidate(), batch, self.val_conf.video, "testing")
            i += 1
        self.assertEqual(i, 1)
        self.assertNotEqual(i, 0)
        print("terminated test_test")

if __name__ == "__main__":
    test = Test()
