import unittest
import torch

from torch.utils.data import DataLoader
from torch.utils.data import random_split

from src.data.dataset import EnvDataset, StaticDataset, CMASampler, ConfEnvDataset
from src.common.env import Env


def custom_collate(env):
    return env

class Test(unittest.TestCase):
    def setUp(self):
        self.dataset = EnvDataset(
            100,
            env_name = "procgen:procgen-coinrun-v0",
        )
        self.static_dataset = StaticDataset(
            size=100,
            batch=5,
            env_name = "procgen:procgen-coinrun-v0",
        )

    def test_len(self):
        self.assertEqual(100, len(self.dataset))

    def test_dataloader(self):
        dataloader = DataLoader(self.dataset, batch_size=5, shuffle=True, collate_fn=custom_collate)
        for batch in dataloader:
            for env in batch:
                env["env"].step(0)

    def test_sampler(self):
        dataset = EnvDataset(
            size=10,
            env_name = "procgen:procgen-coinrun-v0",
        )
        sampler = CMASampler(100, n_envs=10, pop_size=20, batch=10)
        for batch in sampler:
            batch = batch.reshape(-1, 20)
            batch = batch.transpose(1,0)
            for sample in batch:
                self.assertEqual(len(sample), len(set(sample)))

        dataloader = DataLoader(dataset, batch_sampler=sampler, collate_fn=custom_collate)
        for _ in dataloader:
            pass

    def test_split(self):
        dlen = len(self.dataset)
        train, val = random_split(self.dataset, [dlen - 10, 10])
        self.assertEqual(len(train), dlen - 10)
        self.assertEqual(len(val), 10)
        
    def test_static(self):
        dataloader = DataLoader(self.static_dataset, batch_size=5, shuffle=False, collate_fn=custom_collate)
        prev_batch = None
        for batch in dataloader:
            if prev_batch is not None:
                for env1, env2 in zip(prev_batch, batch):
                    self.assertEqual(
                        env1["env"].env.get_info()[0]['level_seed'],
                        env2["env"].env.get_info()[0]['level_seed'],
                    )
            prev_batch = batch

    def test_env(self):
        dataset = ConfEnvDataset(
            100,
            env_name = "procgen:procgen-coinrun-v0",
        )
        dataloader = DataLoader(dataset, batch_size=5, shuffle=True, collate_fn=custom_collate)
        for batch in dataloader:
            for conf in batch:
                env = Env(**conf["conf"])
                env.step(torch.tensor(0))
            pass
        


def main() -> None:
    test = Test()

if __name__ == "__main__":
    main()
