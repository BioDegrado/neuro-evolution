import unittest
import cv2
import gym
import math

import numpy as np
import torch
from torch import nn
from torch import optim
from src.modules.psann import PSANN
from src.modules.actor import Actor


class Test(unittest.TestCase):
    def setUp(self):
        torch.set_default_dtype(torch.double)
        color_gradient = torch.tensor(np.arange(0, 1, 1 / 96))
        r = color_gradient[:, None] + torch.zeros(96)[None, :]
        g = color_gradient[None, :] + torch.zeros(96)[:, None]
        b = torch.zeros(96, 96) + 0.5
        img = torch.stack([b, g, r])
        self.img = img

        ac_space = gym.make("procgen:procgen-bossfight-v0").action_space

        self.model = PSANN(action_space=ac_space)
        self.model_env = PSANN(env_name="MsPacman-v0")
        self.model_box = PSANN(env_name="CarRacing-v0")

    def test_config(self):
        self.assertIsInstance(self.model, Actor)
        self.assertIsInstance(self.model_env, Actor)
        self.assertIsInstance(self.model_box, Actor)

        config = self.model.get_config()
        cfg_env = self.model_env.get_config()
        cfg_box = self.model_box.get_config()

        copy = PSANN(**config)
        cp_env = PSANN(**cfg_env)
        cp_box = PSANN(**cfg_box)

        self.assertDictEqual(copy.get_config(), self.model.get_config())
        self.assertDictEqual(cp_env.get_config(), self.model_env.get_config())
        self.assertDictEqual(cp_box.get_config(), self.model_box.get_config())

    def test_forward_memory(self):
        y = self.model(self.img)

    def test_generatepatches_shape(self):
        unfolder = nn.Unfold(kernel_size=7, stride=4)
        img = self.img
        patches = unfolder(img.view(1, 3, 96, 96))
        patches = patches.view(patches.shape[1], -1)
        patches = torch.einsum("ms->sm", patches)

        self.assertEqual(patches.shape, (529, 147))

    def test_forward(self):
        y = self.model(self.img)
        self.assertEqual(y.shape[0], 15)

        x = self.model.gen_patches(self.img)
        self.model.gen_patches(self.img)
        x = x.transpose(1, 2)  # torch.einsum('bms->bsm', x)
        x = self.model.selfattention(x)
        self.assertNotEqual(
            np.prod(np.array(x[x == 1].detach()).shape), np.prod(x.shape)
        )
        x = x.sum(0)  # torch.einsum('ij->j', x)

    def test_forward_box(self):
        y = self.model_box(self.img)
        self.assertEqual(y.shape[0], 3)

    def test_forward_env(self):
        y = self.model_env(self.img)
        self.assertEqual(y.shape[0], 9)

    def test_parameters_changes(self):
        model = self.model
        params = model.get_parameters()
        model.set_parameters(params)
        params2 = model.get_parameters()
        self.assertTrue(torch.equal(params, params2))

        rparams = torch.randn_like(params2)
        model.set_parameters(rparams)
        oki = model.get_parameters()
        self.assertTrue(torch.equal(rparams.sort()[0], oki.sort()[0]))


def main() -> None:
    test = Test()


if __name__ == "__main__":
    main()
