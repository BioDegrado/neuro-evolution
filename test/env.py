import unittest

import os
import torch
import numpy as np

from src.common import agent
from src.common.env import postprocess_discrete
from src.modules.psann import PSANN
from src.modules.mopsann import MOPSANN

from src.common.env import Env

class Test(unittest.TestCase):
    def setUp(self):
        torch.set_default_dtype(torch.double)
        #  self.env = Env("procgen:procgen-coinrun-v0")
        #  self.env2 = Env("CarRacing-v0")
        #  self.actor = PSANN(
            #  env_name = "procgen:procgen-coinrun-v0",
            #  k_patches = 10,
            #  patch_size = 7,
            #  stride = 4,
            #  hidden_size = 16,
            #  input_size = 96,
            #  selfattention_size = 4,
        #  )
        #  self.actor2 = MOPSANN(
            #  env_name = "procgen:procgen-coinrun-v0", 
            #  k_patches = 10,
            #  patch_size = 7,
            #  stride = 4,
            #  hidden_size = 16,
            #  input_size = 96,
            #  selfattention_size = 4,
        #  )

        #  color_gradient = torch.arange(0, 1, 1/96)
        #  r = color_gradient[:, None] + torch.zeros(96)[None, :]
        #  g = color_gradient[None, :] + torch.zeros(96)[:, None]
        #  b = torch.zeros(96, 96) + 0.5
        #  # img = torch.stack([b, g, r])
        #  img = torch.stack([r, g, b])
        #  # self.img = img
        #  self.img = np.array(torch.einsum("chw->hwc", img))


    def test_minreward(self):
        actor = PSANN(env_name = "CarRacing-v0")
        racing = Env("CarRacing-v0", min_reward=-5)
        state = racing.reset()
        done = False
        while not done:
            self.assertGreaterEqual(racing.ep_reward, racing.min_reward)
            action = agent.get_action(actor, state)
            state, reward, done = racing.step(torch.tensor([0,0,0]))
        print("Reward", racing.ep_reward)

    def test_maxstep(self):
        actor = PSANN(env_name = "CarRacing-v0")
        racing = Env("CarRacing-v0", max_steps=100)
        state = racing.reset()
        done = False
        while not done:
            self.assertLessEqual(racing.steps, racing.max_steps)
            action = agent.get_action(actor, state)
            state, _, done = racing.step(action)
        print("Steps", racing.steps)




if __name__ == "__main__":
    test = Test()
