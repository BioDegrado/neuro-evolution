# Opinions/Rambling

These are just rambling thoughts... and are in no way formal nor necessarily correct!

### The Lottery Ticket

The idea of using self-attention this way, was to get a simple solution while "cheating" the lottery ticket: since apparently sub networks of large models can sometimes outperform the complete counterpart, then why not already training the small network instead? Self-attention should aid this task, since it can facilitate the solution space search, giving a limited area to analyze:

* The self-attention module is usually very small, made of 3 matrices (in our case 2, since we do not use the value matrix).
* In our case the self-attention module gives a very low dimensional output, since we among the $m$ patches we pick $k$ where $k << m$.

### No backprop

The architecture hinders the search, since it has **a non differentiable forward pass**! Meaning that gradient descent cannot be used, instead we are resorting to **CMA**.
On its own is not bad, but this limits the size of the network (too large and it becomes very slow), and makes the network "slower" to train (if you don't have a cluster of PCs). Not all the nearest points are analyzed, so during the "descent" we are analyzing a very limited number of points (it depends on the population, but still, they are few in comparison to the number of parameters)!

### Too easy and too hard

Procgen is a nightmare! The variance of difficulty in the levels is very high! Having levels that are braindead (just jump to the right), while containing somewhat complex level needing complex manoeuvres.
The easiest solution is to just jump to the right, since this way at least the easy level are cleared, plus some accidental complex levels too! If other strategies are too difficult to come up with (low number of parameters, or just very far away, with a rough solution space), then just jump to the right bruh...

### Self-attention and controller

The performance of self-attention and the controller are linked, that's why this architecture works. A good solution point is made of good self-attention configs and good controller configs, HOWEVER! Everything else is discarded as bad point. No matter if the self-attention is pointing to the right areas, if the controller is still retarded, the model is just going to discard the solution point since ultimately it doesn't perform; this also is true in reverse!

#### Counter argument
If for example the self-attention is good then there will be higher chances to succeed the task since we have to configure just the controller.

## Ideas

* An interesting experiment could be to try to feed only levels that cannot be solved by just jumping right! But the better solution is just to increase the population with the number of parameters (self attention or controller).
* Just increase the memory of the controller (by a lot).

### Bad ideas

* Alternate the training of the controller with the training of the self-attention module: fix the parameters of one module and search the parameters of the other for n iterations (or until there are no improvements) and then switch module to train.
	* This can allow a bigger model to train, since we have to train half model each time.
	* If both modules have bad parameters, then the search cannot continue, so maybe start this method after k iterations.
	* CMA doesn't really like this behaviour...
* Rank levels by difficulty? (•ิ_•ิ)??
