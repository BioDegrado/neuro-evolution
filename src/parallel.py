import gym
from typing import Dict
from src.modules.actor import Actor
from src.common import agent
from src.common.env import Env

def init_pool_worker(actor: Actor, device):
    global _actor
    global _env

    _actor = actor.clone().to(device)
    _env = None


def pool_worker(
    params: Dict,
    env_config: Dict,
    target = agent.play_episode,
    *args,
    **kwargs
):
    global _actor
    global _env
    _actor.set_parameters(params)
    if _env is None:
        _env = Env(**env_config)
    else:
        _env.set_seed(env_config["seed"])
    return target(_actor, _env, *args, **kwargs)
