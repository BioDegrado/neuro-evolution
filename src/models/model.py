import abc
import torch
import wandb
import enlighten

from torch import nn
from hydra.utils import log
from typing import Union, Dict
from omegaconf import DictConfig
from torch.utils.data import DataLoader

from src.modules.actor import Actor

class Model(abc.ABC):

    def __init__(self, *args, **kwargs) -> None:
        torch.set_default_dtype(torch.double)

        self.start: int = 0
        self.train_rewards: list = []
        self.val_rewards: list = []
        self.manager = enlighten.get_manager()

        if "use_cuda_if_avail" in kwargs:
            if kwargs["use_cuda_if_avail"]:
                self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
                if self.device.type == "cuda":
                    log.info("Running using GPU")
                else:
                    log.info("GPU not found! Running using CPU")
            else:
                self.device = torch.device("cpu")
                log.info("Running using CPU")
        if "checkpoint" in kwargs:
            self.checkpoint = kwargs["checkpoint"]
        if "validation" in kwargs:
            self.validation = kwargs["validation"]
        if "logger" in kwargs:
            self.logger = kwargs["logger"]


    @abc.abstractmethod
    def save_state(
        self,
        iteration: int,
        train_rewards: list,
        val_rewards: list,
        filename: str = "",
    ) -> None:
        raise NotImplementedError


    @abc.abstractmethod
    def resume(self, state):
        raise NotImplementedError

    @abc.abstractmethod
    def load_state(self, filename: str = "") -> Union[Dict, None]:
        raise NotImplementedError
    
    @abc.abstractmethod
    def get_prodigy(self) -> Union[nn.Module, Actor]:
        raise NotImplementedError

    @abc.abstractmethod
    def test(
        self,
        person: Union[nn.Module, Actor],
        envs: Union[DataLoader, list],
        video: DictConfig,
        prefix: str = "",
    ) -> float:
        raise NotImplementedError

    @abc.abstractmethod
    def training_step(self, envs: list) -> Dict:
        raise NotImplementedError

    @abc.abstractmethod
    def validation_step(self, val_loader, iter) -> Dict:
        raise NotImplementedError

    # Method to handle things at the end of an iteration.
    def postprocess(self, envs, iter):
        pass


    def fit(self, train_loader: DataLoader, val_loader: DataLoader):
        bar = self.manager.counter(total=len(train_loader), desc="Ep.", leave=False)
        for i, train_envs in enumerate(train_loader):

            if i < self.start:
                bar.update()
                continue

            rewards = self.training_step(train_envs)
            self.train_rewards.append(rewards)

            is_sync = wandb.run is not None and wandb.run.step == i
            if not is_sync and self.logger is not None:
                log.info(
                    f"The steps recorded on wandb are {wandb.run.step}, while our steps are {i}"
                    f"Thus we won't record this step."
                )

            if i != 0 and i % self.validation.interval == 0:
                self.val_rewards.append(self.validation_step(val_loader, i))
                if self.logger is not None and is_sync:
                    self.logger.log(self.val_rewards[-1], commit=False)

            post_data = self.postprocess(train_envs, i)
            if post_data is not None:
                self.logger.log(post_data, commit=False)

            if self.logger is not None and is_sync:
                self.logger.log(rewards)

            if i != 0 and i % self.checkpoint.interval == 0:
                self.save_state(i+1, self.train_rewards, self.val_rewards)
            
            bar.update()
