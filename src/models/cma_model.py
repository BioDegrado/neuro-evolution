import cma
import torch
import pickle
import enlighten
import numpy as np
import random as rand
import multiprocessing as mp

from torch import nn
from math import inf
from pathlib import Path
from hydra.utils import log
from typing import Dict, Union
from omegaconf import DictConfig
from torch.utils.data import DataLoader

from src import parallel
from src.common import agent
from src.common.utils import Bar
from src.models.model import Model
from src.modules.actor import Actor


class CMAModel(Model):
    def __init__(self, *args, **kwargs) -> None:
        kwargs = {
                "std_dev": 0.1,
                "population": 256,
                "weight_decay": 0.00001,
                "logger": None,
                "jobs": 1,
                "seed": 0,
                **kwargs
                }
        super().__init__(
            checkpoint = kwargs["checkpoint"],
            validation = kwargs["validation"],
            logger = kwargs["logger"],
            use_cuda_if_avail = True
        )

        seed = kwargs["seed"]
        self.actor_params = kwargs["actor"].get_config()
        self.prodigy = kwargs["actor"].to(self.device)
        self.candidate = self.prodigy.clone()
        self.score = -inf
        self.pop_size = kwargs["population"]
        self.weight_decay = kwargs["weight_decay"]
        self.jobs = kwargs["jobs"]
        self.es = cma.CMAEvolutionStrategy(
            x0 = self.prodigy.get_parameters().detach().numpy(),
            sigma0 = kwargs["std_dev"],
            inopts = {
                #  "CMA_cmean": 0.01,
                "popsize": kwargs["population"],
                "verbose": 0,
                "seed": seed if seed > 0 else rand.randint(0, 2 ** 31)
            }
        )
        if self.checkpoint.resume:
            self.resume(self.load_state())
        else:
            log.info(
                "Resuming disabled, if the checkpoint already exists, then it will be overwritten"
            )


    def resume(self, state):
        if state is not None:
            self.start = state["iteration"]
            self.train_rewards = state["train_rewards"]
            self.val_rewards = state["val_rewards"]
            self.es = state["ev_strat"]
            self.score = state["score"]
            self.prodigy.load_state_dict(state["prodigy"])
        else: 
            log.info("Starting new experment!")


    def save_state(
        self,
        iteration: int,
        train_rewards: list,
        val_rewards: list,
        filename: str = ""
    ):
        if filename == "":
            filename = f"{self.checkpoint.dir}/{self.checkpoint.file}"
        elif Path(filename).is_dir():
            raise FileNotFoundError("The filename belongs to a directory!")
        log.info(f"Saving the checkpoint to {filename}")
        torch.save({
                "prodigy": self.prodigy.state_dict(),
                "score": self.score,
                "iteration": iteration, 
                "train_rewards": train_rewards,
                "val_rewards": val_rewards,
                "ev_strat": self.es,
            }, filename, pickle_protocol=pickle.HIGHEST_PROTOCOL)
        log.info("Checkpoint saved!")


    def load_state(self, filename: str = "") -> Union[Dict, None]:
        if filename == "":
            filename = f"{self.checkpoint.dir}/{self.checkpoint.file}"

        checkpoint = None
        log.info(f"Loading state from {filename}...")
        try:
            checkpoint = torch.load(filename, map_location=self.device)
            log.info(f"State loaded!")
        except FileNotFoundError:
            log.info(f"Checkpoint not found!")
            return None
        except Exception:
            log.info(f"Error in loading the state!")
            return None
        return checkpoint

    
    def get_prodigy(self) -> Union[Actor, nn.Module]:
        return self.prodigy


    def get_candidate(self) -> Union[Actor, nn.Module]:
        self.candidate.set_parameters(self.es.result.xfavorite)
        return self.candidate


    def step(self, params: list, envs: list, args: list, target = agent.play_episode, jobs: int = -1):
        assert isinstance(self.manager, enlighten.Manager)
        jobs = jobs if jobs >= 0 else self.jobs
        data = Bar([
            (param, env["conf"], target, *arg)
            for param,env,arg in zip(params, envs, args)
        ], self.manager, desc="Step", leave = False)
        
        with mp.Pool(
            processes=jobs,
            initializer=parallel.init_pool_worker,
            initargs=(self.prodigy, self.device)
        ) as pool:
            return pool.starmap(
                func=parallel.pool_worker, iterable=data, chunksize=20
            )
        

    def training_step(self, envs: list) -> Dict:
        rewards = []
        params = self.es.ask()
        batch_size = len(envs) // self.pop_size
        _envs = np.array(envs).reshape((-1, self.pop_size))
        bar = self.manager.counter(total=batch_size, desc="Batch", leave=False)
        for batch in _envs:
            rewards.append(
                self.step(params, batch, [[self.weight_decay]] * len(params))
            )
            bar.update()
        bar.close()
        rewards = np.array(rewards)

        rewards = rewards.mean(axis=0)
        self.es.tell(params, -rewards)
        return {
            "max": rewards.max(),
            "avg": rewards.mean(),
            "min": rewards.min(),
            "std_dev": self.es.sigma,
            "rew_dev": rewards.std(),
        }


    def validation_step(self, val_loader, iter):
            log.info("Testing the new candidate...")
            candidate_score = self.test(
                self.get_candidate(), 
                val_loader,
                self.validation.video,
                f"iter_{iter}"
            )
            log.info(f"Testing got an average score of {candidate_score}!")
            if candidate_score >= self.score:
                log.info(f"This is a new record! This is the new prodigy!")
                self.prodigy = self.get_candidate()
                self.score = candidate_score

            return {"val_reward": candidate_score}


    def test(
        self,
        actor: Union[nn.Module, Actor],
        envs: Union[DataLoader, list],
        video: DictConfig,
        prefix: str = "",
    ) -> float:
        assert isinstance(actor, Actor)
        test_rewards: list = []
        if isinstance(envs, DataLoader):
            for batch in envs:
                test_rewards.append(self.test(actor, batch, video, prefix))
        else:
            test_rewards = self.step(
                [actor.get_parameters().tolist()] * len(envs),
                envs,
                [
                    (f"{video.dir}/{prefix}-{i}.avi", video.resolution)
                    for i, _ in enumerate(envs)
                ],
                target=agent.record_episode,
                jobs=2
            )

        return np.array(test_rewards).mean()


    def postprocess(self, envs, iter):
        if iter != 0 and iter % self.validation.interval == 0:
            _envs = np.array(envs).reshape((-1, self.pop_size))
            candidate_score = self.test(
                self.get_candidate(), 
                _envs[:,0],
                self.validation.video,
                f"train_{iter}"
            )
            return {"candidate_training_score": candidate_score}

