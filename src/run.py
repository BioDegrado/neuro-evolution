import os
import hydra
import wandb
from omegaconf import DictConfig
from hydra.utils import log
from torch.utils.data import DataLoader, Dataset
import torch.multiprocessing as mp

from src.common.utils import PROJECT_ROOT
from src.common.utils import (
    seed_everything,
    simple_collate,
    prepend_project_path,
    get_conf_dict,
)
from src.models.model import Model
from src.modules.actor import Actor
from src.data.dataset import CMASampler


def run(cfg: DictConfig):
    # log.info(f"Sharing strategy: {mp.get_sharing_strategy()}")
    if cfg.train.deterministic:
        log.info("Deterministic mode activated!")
        seed_everything(cfg.train.random_seed)

    if cfg.train.debug:
        log.info(
            f"Debug mode <{cfg.train.debug}>. "
            f"Forcing debugger friendly configuration!"
        )

        # Switch wandb mode to offline to prevent online logging
        cfg.logging.wandb.mode = "offline"
        cfg.train.checkpoint.resume = False
        cfg.logging.wandb.resume = False
        cfg.train.checkpoint.file = "test.check"

    if not cfg.train.checkpoint.resume:
        cfg.logging.wandb.resume = False

    cfg.train.checkpoint.dir = prepend_project_path(cfg.train.checkpoint.dir)
    cfg.train.val.video.dir = prepend_project_path(cfg.train.val.video.dir)

    os.makedirs(cfg.train.checkpoint.dir, exist_ok=True)
    os.makedirs(cfg.train.val.video.dir, exist_ok=True)

    # Logger instantiation/configuration
    wandb_logger = None
    if "wandb" in cfg.logging:
        log.info(f"Instantiating <WandbLogger>")
        wandb_config = cfg.logging.wandb
        log.info(f"The config sent to Wandb is:")
        for k, i in get_conf_dict(cfg).items():
            log.info(f"\t{k}: {i}")
        wandb_logger = wandb.init(
            **wandb_config, tags=cfg.core.tags, config=get_conf_dict(cfg)
        )

    episodes = cfg.train.episodes
    population = cfg.train.population
    batch = cfg.train.reps
    n_envs = cfg.data.train.n_envs
    log.info(f"Instantiating datasets")
    sampler = CMASampler(episodes, n_envs, population, batch)
    train_dataset: Dataset = hydra.utils.instantiate(
        cfg.data.train,
        env_name=cfg.data.env_name,
        size=episodes,
    )
    train_loader = DataLoader(
        train_dataset,
        batch_sampler=sampler,
        collate_fn=simple_collate,
    )
    val_dataset: Dataset = hydra.utils.instantiate(
        cfg.data.val,
        env_name=cfg.data.env_name,
    )
    val_loader = DataLoader(
        val_dataset, batch_size=len(val_dataset), collate_fn=simple_collate
    )
    log.info(f"Instatiating actor...")
    actor: Actor = hydra.utils.instantiate(cfg.model.nn, env_name=cfg.data.env_name)
    log.info(f"The actor has {len(actor.get_parameters())} parameters.")

    # Instantiate model
    ## Hack to not let "instantiate" the neural network
    cfg.model.nn = None
    log.info(f"Instantiating <{cfg.model._target_}>")
    model: Model = hydra.utils.instantiate(
        cfg.model,  # path of the model, and other parameters
        actor=actor,
        env_name=cfg.data.env_name,
        population=population,
        checkpoint=cfg.train.checkpoint,
        validation=cfg.train.val,
        reps=batch,
        logger=wandb_logger,
    )

    log.info(f"Starting training!")
    model.fit(train_loader, val_loader)


@hydra.main(config_path=str(PROJECT_ROOT / "conf"), config_name="default")
def main(cfg: DictConfig):
    run(cfg)


if __name__ == "__main__":
    mp.set_start_method("fork")
    main()
