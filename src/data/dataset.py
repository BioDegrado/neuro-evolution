import random
import numpy as np
import gym
from torch.utils.data import Dataset, Sampler

# It doesn't guarantee that all elementes in the dataset are going to be used!
class CMASampler(Sampler):
    def __init__(self, episodes: int, n_envs: int,  pop_size: int, batch: int):
        if batch < 0:
            raise ValueError("The batch size must be a positive integer!")
        if episodes < 0:
            raise ValueError("The episodes must be a positive integer!")
        if n_envs < 0:
            raise ValueError("n_envs must be a positive integer!")
        if pop_size < 0:
            raise ValueError("The population size must be a positive integer!")
        envs = np.arange(n_envs) if n_envs else batch
        self.indices = np.array([
                [
                    np.random.permutation(envs)[:batch]
                    for _ in range(pop_size)
                ]
                for _ in range(episodes)
            ]
        )
        self.indices = self.indices.transpose(0, 2, 1)
        self.indices = self.indices.reshape(episodes, -1)

    def __len__(self):
        return len(self.indices)

    def __iter__(self):
        return iter(self.indices)


class EnvDataset(Dataset):
    def __init__(self, size: int, env_name: str, n_envs: int = 0, **opts):
        n_envs = n_envs if n_envs != 0 else size
        self.seeds = random.sample(range(0, 2 ** 31 - 1), n_envs)
        self.size = size
        self.env_params = {**opts, "id": env_name}

    
    def __len__(self):
        return self.size
        

    def __getitem__(self, idx):
        idx = idx % len(self.seeds)
        conf = {**self.env_params, "seed": self.seeds[idx]}
        try:
            env = gym.make(**self.env_params, start_level=self.seeds[idx])
        except TypeError:
            env = gym.make(**self.env_params)
            env.seed(idx)
        return {
            "env": env,
            "conf": conf
        }


class RandomConfEnvDataset(EnvDataset):
    def __init__(self, size: int, env_name: str, n_envs: int = 0, **opts):
        super().__init__(size, env_name, 1, **opts)

    def __getitem__(self, idx):
        seed = random.randint(0, 2 ** 31 - 1)
        conf = {**self.env_params, "seed": seed}
        return {"conf": conf}


class ConfEnvDataset(EnvDataset):
    def __init__(self, size: int, env_name: str, n_envs: int = 0, **opts):
        super().__init__(size, env_name, n_envs, **opts)

    def __getitem__(self, idx):
        idx = idx % len(self.seeds)
        conf = {**self.env_params, "seed": self.seeds[idx]}
        print(conf)
        return {"conf": conf}


class StaticDataset(Dataset):
    def __init__(self, size: int, env_name: str, batch: int = 1, **opts):
        self.seeds = random.sample(range(0, 2 ** 31 - 1), batch)
        self.size = size
        self.env_params = {"num_levels": 1, **opts, "id": env_name}
    

    def __len__(self):
        return self.size


    def __getitem__(self, idx):
        conf = {**self.env_params, "seed": self.seeds[idx % len(self.seeds)]}
        try:
            env = gym.make(**self.env_params, start_level=self.seeds[idx % len(self.seeds)])
        except Exception:
            env = gym.make(**self.env_params)
            env.seed(idx % len(self.seeds))
        return {
            "env": env,
            "conf": conf
        }
