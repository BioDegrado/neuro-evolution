import os
import torch
import wandb
import argparse
from hydra.utils import log



def main(project, run, checkpoint):
    log.info(f"Instantiating <WandbLogger>")
    wandb_logger = wandb.init(
        project=project,
        resume=run,
        mode="online"
    )

    checkpoint = torch.load(checkpoint, map_location=torch.device("cpu"))
    log.info(f"State loaded!")

    train_rewards = checkpoint["train_rewards"]
    val_rewards = checkpoint["val_rewards"]

    for i, t_reward in enumerate(train_rewards):
        wandb_logger.log(t_reward)
        if i != 0 and i % 10 == 0:
            wandb_logger.log({"val_rewards": val_rewards[i // 10]})
            
    # Logger closing to release resources/avoid multi-run conflicts
    if wandb_logger is not None:
        wandb_logger.experiment.finish()


if __name__ == "__main__":
    desc = "Compress the input video file, with different degree of compression and outputs a csv file with SSIM."
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument(
        "project",
        type=str,
        help="The project name in wandb"
    )
    parser.add_argument(
        "run",
        type=str,
        help="The name of the run to dump the infos"
    )
    parser.add_argument(
        "checkpoint",
        type=str,
        help="The number of compressions to do."
    )
    args = parser.parse_args()
    main(args.project, args.run, args.checkpoint)
