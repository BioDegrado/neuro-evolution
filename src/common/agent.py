import cv2
import torch
import numpy as np
from torch import nn
from typing import Union
from torchvision import transforms

from src.modules.actor import Actor
from src.common.env import Env


def l2(t: torch.Tensor) -> float:
    return float(torch.dot(t, t))


_to_tensor = transforms.Compose([transforms.ToTensor()])


def get_action(actor: nn.Module, state: np.ndarray) -> Union[int, np.ndarray]:
    return actor(_to_tensor(state.copy()))


def play_episode(
    actor: Actor, env: Env, weight_decay=0.00001, eval: bool = False
) -> float:
    actor.reset_memory()
    done = False
    state = env.reset()
    while not done:
        action = get_action(actor, state)
        state, _, done = env.step(action)
    if eval:
        return env.ep_reward
    else:
        return env.ep_reward - weight_decay * l2(actor.get_parameters())


def record_episode(
    actor: Actor,
    env: Env,
    record_file: str,
    resolution: int = 500,
) -> float:
    out = cv2.VideoWriter(
        record_file,
        fourcc=cv2.VideoWriter_fourcc(*"DIVX"),
        fps=30,
        frameSize=(resolution, resolution),
    )
    done = False
    actor.reset_memory()
    state = env.reset()
    while not done:
        action = get_action(actor, state)
        state, _, done = env.step(action)
        action = env.process_action(action)
        img = state
        img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
        img = actor.record_postprocess(img, action)
        img = cv2.resize(img, (resolution, resolution), interpolation=cv2.INTER_NEAREST)
        out.write(img)
    out.release()
    return env.ep_reward
