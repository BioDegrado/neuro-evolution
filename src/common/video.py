import cv2
import numpy as np
from gym.spaces import Discrete, Box

from src.common.procgen import ACTIONS as PG_ACTIONS
from src.common.procgen import ACTIONS_ARROWKEYS as PG_AA
from src.common.procgen import BUTTONS as PG_BUTTONS

OFFSET = 0.05
BUTTON_SIZE=0.04
SPACINGX = 0.5
SPACINGY = 0.1
SPACINGA = 0.1
button_rec = []

pt1 = [0, 0]
pt2 = [0, 0]
for i in range(12):
    if i == 3 or i == 5:
        continue
    is_action = (i%6) < 3
    action_padding = 0 if is_action else SPACINGA
    x = OFFSET + action_padding + ((i%6) / 6) * SPACINGX
    y = OFFSET + (i // 6) * SPACINGY
    pt1 = np.array((x ,y))
    pt2 = np.array((x + BUTTON_SIZE, y + BUTTON_SIZE))
    button_rec.append((pt1, pt2))


def draw_patches(img, rectangles, offset, r_scale, colors = None) -> np.ndarray:
    if colors is None:
        colors = np.array([[0, 1, 0] for _ in rectangles]) * 255
    else:
        colors = np.array(colors) * 255

    for (y,x),c in zip(rectangles, colors):
        x = int(x.item() * r_scale)
        y = int(y.item() * r_scale)
        pt1 = (x - offset, y - offset)
        pt2 = (x + offset, y + offset)
        bgr = [128, 128, 128]
        for i, v in enumerate(c):
            bgr[2 - i] = int(v)
        cv2.rectangle(img, pt1, pt2, color=bgr, thickness=1)
    return img


def draw_controls(img, action, action_space, procgen: bool = False) -> np.ndarray:
    # print("Procgen:", procgen)
    if type(action_space) == Discrete:
        w, h, c = img.shape
        color = [128, 128, 128]
        color2 = [256, 256, 256]

        if procgen:
            pressed = []
            for button in PG_ACTIONS[action]:
                pressed.append(PG_BUTTONS[button])
            for i, rec in enumerate(button_rec):
                pt1 = (rec[0]*w).astype(np.int32)
                pt2 = (rec[1]*w).astype(np.int32)
                if i in pressed:
                    img = cv2.rectangle(img, pt1, pt2, color2, -1)
                else:
                    img = cv2.rectangle(img, pt1, pt2, color, -1)
        else:
            return img
    # To implement
    if type(action_space) == Box:
        return img
    return img

