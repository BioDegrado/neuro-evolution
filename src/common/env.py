import gym
import random
import numpy as np
from torch import argmax
from gym.spaces import Discrete, Box


def postprocess_discrete(output):
    return argmax(output).item()

def postprocess_simple(output) -> np.ndarray:
    return np.array(output)


class Env():
    def __init__(
        self,
        id: str,
        seed: int = -1,
        max_steps: int = 0,
        min_reward: int = 0,
        **opts
    ):
        self.id = id
        self.seed = seed if seed > -1 else random.randint(0, 2 * 31 - 1)
        self.opts = opts
        self.procgen = id.startswith("procgen")

        if self.procgen:
            self.env = gym.make(id, start_level=self.seed, num_levels=1, **opts)
        else:
            self.env = gym.make(id, **opts)
            self.env.seed(self.seed)

        self.max_steps = max_steps
        self.min_reward = min_reward

        self.steps = 0
        self.ep_reward = 0

        self.action_space = self.env.action_space
        if type(self.env.action_space) == Discrete:
            self.process_action = postprocess_discrete
        elif type(self.env.action_space) == Box:
            self.process_action = postprocess_simple


    def reset(self):
        self.steps = 0
        self.ep_reward = 0
        if self.procgen:
            self.env.reset()
            state = self.env.env.observe()[1]
            return state.squeeze()
        else:
            return self.env.reset()


    def set_seed(self, seed: int):
        self.seed = seed if seed > -1 else random.randint(0, 2 * 31 - 1)
        if self.procgen:
            self.env = gym.make(self.id, start_level=self.seed, num_levels=1, **self.opts)
        else:
            self.env.seed(self.seed)


    def step(self, action):
        action = self.process_action(action)
        state, reward, done, _ = self.env.step(action)
        self.steps += 1
        self.ep_reward += reward
        too_many_steps = self.max_steps and self.steps > self.max_steps 
        too_few_rewards = self.min_reward and self.ep_reward < self.min_reward
        done = done or too_many_steps or too_few_rewards
        return state, reward, done
