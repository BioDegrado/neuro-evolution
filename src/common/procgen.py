ACTIONS = [
    ("LEFT", "DOWN"),
    ("LEFT",),
    ("LEFT", "UP"),
    ("DOWN",),
    (),
    ("UP",),
    ("RIGHT", "DOWN"),
    ("RIGHT",),
    ("RIGHT", "UP"),
    ("D",),
    ("A",),
    ("W",),
    ("S",),
    ("Q",),
    ("E",),
]

ACTIONS_ARROWKEYS = [
    ("LEFT",),
    ("LEFT", "UP"),
    (),
    ("UP",),
    ("RIGHT",),
    ("RIGHT", "UP"),
]

BUTTONS = {
    "Q":0,
    "W":1,
    "E":2,
    "UP":3,
    "A":4,
    "S":5,
    "D":6,
    "LEFT":7,
    "DOWN":8,
    "RIGHT":9,
}
