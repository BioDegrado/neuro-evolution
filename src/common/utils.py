import os
import torch
import dotenv
import random
import numpy as np
from pathlib import Path
from typing import Optional, Dict
from omegaconf import DictConfig
import enlighten as en
from typing import Iterable, Union


class Bar:
    def __init__(
        self, iterable: Iterable, manager: en.Manager, **kwargs
    ):
        self.iterable = iterable
        self.bar = manager.counter(total=len(self), **kwargs)
        self.closed = False

    def __iter__(self):
        try:
            for obj in self.iterable:
                yield obj
                self.bar.update()
        finally:
            self.bar.close()
            self.closed = True
        return

    def __len__(self):
        return len(self.iterable)

    def __del__(self):
        if not self.closed:
            self.bar.close()


def prepend_project_path(path):
    path = path if os.path.isabs(path) else PROJECT_ROOT / path
    return str(path)


def simple_collate(env):
    return env


def flatten_dict(dict: DictConfig):
    new_dict: Dict = {}
    for k,v in dict.items():
        if type(v) == DictConfig:
            for fk,fv in flatten_dict(v).items():
                new_dict[f"{k}.{fk}"] = fv
        else:
            new_dict[k] = v
    return new_dict


def get_conf_dict(cfg: DictConfig) -> Dict:
    return flatten_dict(cfg)


def seed_everything(seed=42):
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)


def get_env(env_name: str, default: Optional[str] = None) -> str:
    """
    Safely read an environment variable.
    Raises errors if it is not defined or it is empty.

    :param env_name: the name of the environment variable
    :param default: the default (optional) value for the environment variable

    :return: the value of the environment variable
    """
    if env_name not in os.environ:
        if default is None:
            raise KeyError(f"{env_name} not defined and no default value is present!")
        return default

    env_value: str = os.environ[env_name]
    if not env_value:
        if default is None:
            raise ValueError(
                f"{env_name} has yet to be configured and no default value is present!"
            )
        return default

    return env_value


def load_envs(env_file: Optional[str] = None) -> None:
    """
    Load all the environment variables defined in the `env_file`.
    This is equivalent to `. env_file` in bash.

    It is possible to define all the system specific variables in the `env_file`.

    :param env_file: the file that defines the environment variables to use. If None
                     it searches for a `.env` file in the project.
    """
    dotenv.load_dotenv(dotenv_path=env_file, override=True)


STATS_KEY: str = "stats"


# Load environment variables
load_envs()

# Set the cwd to the project root
# With expand user we can use the keyword ~ to alias the home directory.
PROJECT_ROOT: Path = Path(get_env("PROJECT_ROOT")).expanduser()
assert (
    PROJECT_ROOT.exists()
), "You must configure the PROJECT_ROOT environment variable in a .env file!"

os.chdir(PROJECT_ROOT)
