import cv2
import gym
import torch
import numpy as np

from torch import nn
from torchvision import transforms
from gym.spaces.discrete import Discrete
from gym.spaces.box import Box

from src.modules.actor import Actor
from src.modules.modules import AddBatchDim
from src.modules.selfattention import SelfAttentionScore
from src.modules.centers import TopCenters
from src.common import video

class PatchClassifier(nn.Module):
    def __init__(self, channel, input_size, output_size):
        super().__init__()
        self.input_size = input_size
        self.channel = channel
        #  self.layers = nn.Sequential( # b s s c
            #  nn.Linear(input_size * input_size * channel, input_size * input_size * channel ),
            #  nn.ReLU(),
            #  nn.Linear(input_size * input_size , output_size),
            #  nn.Sigmoid()
        #  )
        hidden_size = (input_size - 2 - 2)**2
        self.layers = nn.Sequential( # b s s c
            nn.Conv2d(channel, 5, (3, 3)),
            nn.ReLU(),
            nn.Conv2d(5, 5, (3, 3)),
            nn.ReLU(),
            nn.Flatten(),
            nn.Linear(5 * hidden_size , output_size),
            nn.Sigmoid()
        )
        self.classes = torch.eye(output_size)


    def forward(self, x):
        x = x.view(-1, self.channel, self.input_size, self.input_size)
        x = self.layers(x)
        idx = x.argmax(dim=1)
        x = self.classes[idx]
        return x


class MOPSANN(Actor):
    def __init__(self, **kwargs):
        super().__init__()
        torch.set_default_dtype(torch.double)
        defaults = {
            "input_size": 96,
            "selfattention_size": 4,
            "k_patches": 10,
            "patch_size": 7,
            "stride": 4,
            "hidden_size": 16,
            "classes": 3,
            "num_layers": 1,
        }
        kwargs = { **defaults, **kwargs }
        self.config = kwargs
        self.procgen = False
        if (not "action_space" in kwargs) and (not "env_name" in kwargs):
            raise ValueError("Either action_space or env_name must be passed!")
        if "env_name" in kwargs:
            env = gym.make(kwargs["env_name"])
            kwargs["action_space"] = env.action_space
            if kwargs["env_name"].startswith("procgen"):
                self.procgen = True


        self.input_size = kwargs["input_size"]
        self.k_patches = kwargs["k_patches"]
        self.stride = kwargs["stride"]
        self.hidden_size = kwargs["hidden_size"]
        self.patch_size = kwargs["patch_size"]
        self.num_layers = kwargs["num_layers"]
        self.patch_size = kwargs["patch_size"]
        self.action_space = kwargs["action_space"]
        classes = kwargs["classes"]

        self.gen_patches = nn.Sequential(
            transforms.Resize((self.input_size, self.input_size)),
            AddBatchDim(),
            nn.Unfold(kernel_size=self.patch_size, stride=self.stride),
            nn.Flatten(start_dim=2),
        )
        # self.preprocess = torch.jit.script(self.preprocess)

        # Self attention module
        m = self.patch_size * self.patch_size * 3
        d = kwargs["selfattention_size"]
        self.selfattention = SelfAttentionScore(m, d)
        self.top_centers = TopCenters(self.k_patches, self.input_size, self.patch_size, self.stride)
        self.patch_classifier = PatchClassifier(3, self.patch_size, classes)

        self.hidden = (
            torch.zeros((self.num_layers, 1, self.hidden_size)),
            torch.zeros((self.num_layers, 1, self.hidden_size)),
        )
        self.rnn = torch.nn.LSTM(
            input_size = self.k_patches * (2 + classes), 
            num_layers = self.num_layers,
            hidden_size = self.hidden_size
        )

        if type(self.action_space) == Discrete:
            n_actions = self.action_space.n
            self.output = nn.Sequential(
                nn.Linear(self.hidden_size, n_actions),
                nn.Softmax(dim=-1),
            )
        elif type(self.action_space) == Box: 
            n_actions = self.action_space.shape[0]
            self.output = nn.Sequential(
                nn.Linear(self.hidden_size, n_actions),
                nn.Tanh(),
            )
        else:
            raise TypeError(
                f"action_space has a type that is not supported: {type(self.action_space)}"
            )


    def clone(self):
        return MOPSANN(**self.config)


    def reset_memory(self, device=None):
        device = self.get_device() if device is None else device
        self.hidden = (
            torch.zeros((self.num_layers, 1, self.hidden_size)).to(device),
            torch.zeros((self.num_layers, 1, self.hidden_size)).to(device)
        )


    def to(self, device):
        super(Actor, self).to(device=device)
        self.reset_memory(device)
        return self
        

    @torch.no_grad()
    def forward(self, x: torch.Tensor) -> torch.Tensor:
        device = self.get_device()
        x = x.to(device)
        x = self.gen_patches(x)
        x = x.transpose(1,2) # torch.einsum('bms->bsm', x)

        patches = x.squeeze()

        scores = self.selfattention(x)
        centers = self.top_centers(scores).to(device)

        # patches = patches[self.top_centers.top_idx].to(device)
        patches = patches[self.top_centers.top_idx].to(device)
        classes = self.patch_classifier(patches)
        self.k_patches_cla = classes

        x = torch.cat((centers.to(device), classes), dim=1).view(1,1,-1).to(device)

        x, self.hidden = self.rnn(x, self.hidden)
        return self.output(x.view(-1))

    def record_postprocess(self, state: np.ndarray, action) -> np.ndarray:
        state = cv2.resize(
            state,
            (self.input_size, self.input_size),
            interpolation=cv2.INTER_NEAREST
        )
        state = video.draw_patches(
            state,
            self.top_centers.positions,
            self.top_centers.offset,
            self.input_size,
            self.k_patches_cla
        )
        state = video.draw_controls(state, action, self.action_space, self.procgen)
        return state
