import abc
import torch
import numpy as np
from torch import nn
from typing import Dict


class Actor(abc.ABC, nn.Module):

    config: Dict = {}

    def set_parameters(self, params):
        if type(params) is not torch.Tensor:
            params = torch.tensor(params)

        #  if len(params) != len(self.get_parameters()):
        #  raise ValueError(
        #  f"The parameters are mismatched!"
        #  f"There are {len(params)} parameters in input!"
        #  f"While the network have {len(self.get_parameters())} parameters!"
        #  )

        start = 0
        for neurons in self.parameters():
            end = start + np.prod(neurons.shape)
            neurons.data = params[start:end].view(neurons.shape).to(neurons.device)
            start = end

    def get_parameters(self) -> torch.Tensor:
        params = [p.view(-1) for p in self.parameters()]
        if not params:
            return torch.tensor([])
        return torch.cat(params)

    def get_config(self):
        return self.config

    def get_device(self) -> torch.device:
        return next(self.parameters()).device

    def record_postprocess(self, state: np.ndarray, action) -> np.ndarray:
        return state

    @abc.abstractmethod
    def clone(self):
        raise NotImplementedError
