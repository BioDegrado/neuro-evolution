import torch
import gym
from gym.spaces.box import Box
from gym.spaces.discrete import Discrete

from src.modules.actor import Actor

class Repeat(Actor):
    def __init__(self, **kwargs):
        super().__init__()
        self.config = kwargs
        if (not "action_space" in kwargs) and (not "env_name" in kwargs):
            raise ValueError("Either action_space or env_name must be passed!")
        if "env_name" in kwargs:
            env = gym.make(kwargs["env_name"])
            kwargs["action_space"] = env.action_space
        if not "action" in kwargs:
            raise ValueError("Random(Actor) needs the action to repeat")
        self.action = torch.tensor(kwargs["action"])

        if type(kwargs["action_space"]) == Discrete:
            self.n = kwargs["action_space"].n
        elif type(kwargs["action_space"]) == Box: 
            self.n = kwargs["action_space"].shape[0]
        else:
            raise TypeError(f"action_space has a type that is not supported: {type(kwargs['action_space'])}")
        if self.action.shape != (self.n, ):
            raise ValueError("Action different from action space")


    def clone(self):
        return Repeat(**self.config)

    def reset_memory(self, device=None):
        pass
        

    @torch.no_grad()
    def forward(self, x: torch.Tensor) -> torch.Tensor:
        return self.action
