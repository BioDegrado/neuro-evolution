import torch
from torch import nn


class AddBatchDim(nn.Module):
    def __init__(self):
        super().__init__()

    def forward(self, x):
        return x.view(1, *x.shape)

class DelBatchDim(nn.Module):
    def __init__(self):
        super().__init__()

    def forward(self, x):
        return x.view(*x.shape[1:])
