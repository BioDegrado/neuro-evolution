import torch
import math
from torch import nn

class SelfAttentionScore(nn.Module):
    def __init__(self, m: int, d: int):
        super().__init__() 
        self.keys    = nn.Linear(m, d)
        self.queries = nn.Linear(m, d)
        self.softmax = nn.Softmax(dim=-1)

    def forward(self, x):
        m = x.shape[2]
        keys = self.keys(x)
        queries = self.queries(x)
        x = torch.bmm(queries, keys.transpose(1,2)) / math.sqrt(m) # einsum: 'bsm,bmz->bsz'
        x = self.softmax(x.squeeze())
        x = x.sum(0) # torch.einsum('ij->j', x)
        return x
    
