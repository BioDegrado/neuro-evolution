import torch
import math
from torch import nn


class TopCenters(nn.Module):
    def __init__(self, k :int, input_size: int, patch_size: int, stride: int):
        super().__init__() 
        centers = []
        n_patches = math.floor(
            (input_size - patch_size) / stride
        ) + 1
        offset = patch_size // 2
        for i in range(n_patches):
            row = offset + i * stride
            for j in range(n_patches):
                col = offset + j * stride
                centers.append((row, col)) 
        self.centers = torch.tensor(centers, dtype=torch.double) / input_size
        self.offset = offset
        self.positions = None
        self.k = k


    def forward(self, x):
        sorted_idx = torch.argsort(x, descending=True)
        self.top_idx = sorted_idx[:self.k]
        self.positions = self.centers[self.top_idx]
        return self.positions


    def to(self, device):
        super().to(device=device)
        self.centers.to(device=device)
        return self
