import cv2
import gym
import torch
import numpy as np

from torch import nn
from torchvision import transforms
from gym.spaces.discrete import Discrete
from gym.spaces.box import Box

from src.modules.actor import Actor
from src.modules.modules import AddBatchDim
from src.modules.selfattention import SelfAttentionScore
from src.modules.centers import TopCenters
from src.common import video


class PSANN(Actor):
    def __init__(self, **kwargs):
        super().__init__()
        torch.set_default_dtype(torch.double)
        kwargs = {
            "input_size": 96,
            "selfattention_size": 4,
            "k_patches": 10,
            "patch_size": 7,
            "stride": 4,
            "hidden_size": 16,
            "num_layers": 1,
            **kwargs
        }
        self.config = kwargs
        self.procgen = False
        if (not "action_space" in kwargs) and (not "env_name" in kwargs):
            raise ValueError("Either action_space or env_name must be passed!")
        if "env_name" in kwargs:
            env = gym.make(kwargs["env_name"])
            kwargs["action_space"] = env.action_space
            if kwargs["env_name"].startswith("procgen"):
                self.procgen = True

        self.input_size = kwargs["input_size"]
        self.k_patches = kwargs["k_patches"]
        self.stride = kwargs["stride"]
        self.hidden_size = kwargs["hidden_size"]
        self.patch_size = kwargs["patch_size"]
        self.num_layers = kwargs["num_layers"]
        self.action_space = kwargs["action_space"]

        self.gen_patches = nn.Sequential(
            transforms.Resize((self.input_size, self.input_size)),
            AddBatchDim(),
            nn.Unfold(kernel_size=self.patch_size, stride=self.stride),
            nn.Flatten(start_dim=2),
        )
        # self.preprocess = torch.jit.script(self.preprocess)

        # Self attention module
        m = (self.patch_size ** 2) * 3
        d = kwargs["selfattention_size"]
        self.selfattention = SelfAttentionScore(m, d)
        self.top_centers = TopCenters(self.k_patches, self.input_size, self.patch_size, self.stride)

        self.hidden = (
            torch.zeros((self.num_layers, 1, self.hidden_size)),
            torch.zeros((self.num_layers, 1, self.hidden_size)),
        )
        self.lstm = torch.nn.LSTM(
            input_size = self.k_patches * 2, 
            num_layers = self.num_layers,
            hidden_size = self.hidden_size
        )

        if type(self.action_space) == Discrete:
            n_actions = self.action_space.n
            self.output = nn.Sequential(
                nn.Linear(self.hidden_size, n_actions),
                nn.Tanh(),
            )
        elif type(self.action_space) == Box: 
            n_actions = self.action_space.shape[0]
            self.output = nn.Sequential(
                nn.Linear(self.hidden_size, n_actions),
                nn.Tanh(),
            )
        else:
            raise TypeError(
                f"action_space has a type that is not supported: {type(self.action_space)}"
            )


    def clone(self):
        return PSANN(**self.config)


    def reset_memory(self, device=None):
        device = self.get_device() if device is None else device
        self.hidden = (
            torch.zeros((self.num_layers, 1, self.hidden_size)),
            torch.zeros((self.num_layers, 1, self.hidden_size)),
        )


    def to(self, device):
        super(Actor, self).to(device=device)
        self.reset_memory(device)
        return self
        

    @torch.no_grad()
    def forward(self, x: torch.Tensor) -> torch.Tensor:
        device = self.get_device()
        x = x.to(device)
        x = self.gen_patches(x)
        x = x.transpose(1,2) # torch.einsum('bms->bsm', x)
        scores = self.selfattention(x)
        centers = self.top_centers(scores).to(device) # Breaks the gradient. It interrupts the operation chain!
        x, self.hidden = self.lstm(
            centers.view(1,1,-1), self.hidden
        )
        return self.output(x.view(1,-1)).squeeze()


    def record_postprocess(self, state: np.ndarray, action) -> np.ndarray:
        state = cv2.resize(
            state,
            (self.input_size, self.input_size),
            interpolation=cv2.INTER_NEAREST
        )
        state = video.draw_patches(
            state,
            self.top_centers.positions,
            self.top_centers.offset,
            self.input_size,
            None
        )
        state = video.draw_controls(state, action, self.action_space, self.procgen)
        return state
