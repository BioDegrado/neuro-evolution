# Neuro Evolution

<p align="center">
    <a href="https://pytorch.org/get-started/locally/"><img alt="PyTorch" src="https://img.shields.io/badge/-PyTorch-red?logo=pytorch&labelColor=gray"></a>
    <a href="https://hydra.cc/"><img alt="Conf: hydra" src="https://img.shields.io/badge/conf-hydra-blue"></a>
    <a href="https://wandb.ai/site"><img alt="Logging: wandb" src="https://img.shields.io/badge/logging-wandb-yellow"></a>
</p>

![](img/train.gif)

This is a reimplementation of the [Attention Agent Model](https://github.com/RonghuiZhou/brain-tokyo-workshop) proposed on the Tokyo-Brain workshop.  
The code architecture is mainly based on the [NN Template](https://github.com/lucmos/nn-template), however due to different implementation requirements, this code base has being implemented without Pytorch Lightning, but just Pytorch.

[[_TOC_]]

# Installation

```bash
git clone https://gitlab.com/BioDegrado/neuro-evolution.git
cd neuro-evolution
pip install -r requirements.txt
```
Due to Procgen not supporting the last versions of Python, I suggest to create a virtual environment with Python <= 3.10.
More infos [here](https://github.com/openai/procgen).

In the "run" folder there are a couple script for running the models (training).

# Differences to [Attention Agent Model](https://github.com/RonghuiZhou/brain-tokyo-workshop)

This project doesn't support clustering, leading to a possibly way slower training than the original counterpart. 
The models are reimplemented in a different way, however the base model behave as the original model.

Other differences:

* It supports the Procgen environments by default.
* It shows the input of the controller to the screen by default (Only Procgen for now).
* The base architecture of the Network has remained basically the same, it is renamed as PSANN (Positional Self-Attention Neural Network)... bad name I know.
* Implementation of a personal version of PSANN, MOPSANN(Multi Object PSANN); that is, just a PSANN with an additional classifier of patches.
* The recording of videos now can display the controls (Procgen only at the moment).
* Recording of runs.

# Results

The performance is... not good. It seems that both the original model (PSANN) and the modified one (MOPSANN) are underfitting.

<img src=img/avg.png width=30% height=30%>

Moreover the modification is hindering the training.
This is possibly due to both being **very small networks**, since a smaller network was also tested against Procgen, providing similar results.

The modified model has shown worse results than the original counterpart. Although, it showed some resembalnce of learning, it still performed badly, with the class of patches having no intuitive interpretation to them... from looking at the runs it seems that the "red" class if it's the majority of patches it triggers the jump action.
