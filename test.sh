#!/bin/sh

TEST='python -m unittest'
failed="false"
for unittest in test/*.py; do
	module=${unittest%.py}
	module=${module//\//.}
	[ $module = '__init__' ] && continue
	if ! $TEST $module; then
		failed="true"
	fi
done
echo "At least 1 test has failed!"
